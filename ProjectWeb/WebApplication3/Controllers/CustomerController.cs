﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using projpart1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {

        private void AuthenticateAndGetTokenAndGetFacade(out LoginToken<Customer> token_customer, 
            out CustomerFacade facade)
        {
            // after we learned authentication
            // 1. validate token
            // 2. retrieve LoginToken<Customer>
            // 3. get Customer facade

            // before we learn authentication
            // 1. perform login  -- use real user-name + pwd
            // 2. get the token + facade
            FlightCenterSystem.Login("itay", "1234", out FacadeBase facadeBase, out ILoginToken token);
            token_customer = token as LoginToken<Customer>;
            facade = facadeBase as CustomerFacade;
        }

        // GET api/<ProductController>/5
        [HttpGet("getalltickets")]
        public List<Ticket> GetAllTickets() // TOKEN
        {
            // 1. AuthenticateAndGetTokenAndGetFacade()
            // 2. var result = customerFacade.GetAllTickets(token);
            // 3. return resut;
            AuthenticateAndGetTokenAndGetFacade(out LoginToken<Customer> token_customer, out CustomerFacade facade);
            var result = facade.GetAllTickets(token_customer);
            return result;
        }

        [HttpGet("getflight/{flight_id}")]
        public async Task<ActionResult<Flight>> GetFlightById(int flight_id) // TOKEN
        {
            // 1. AuthenticateAndGetTokenAndGetFacade()
            // 2. var result = customerFacade.GetAllTickets(token);
            // 3. return resut;
            AuthenticateAndGetTokenAndGetFacade(out LoginToken<Customer> token_customer, out CustomerFacade facade);

            // 1. what if flight_id is minus or zero?

            // 2. what if no flight found?
            // 3. correct result
            Flight result = null;
            try
            {
                result = await Task.Run(() => facade.GetFlightById(flight_id));
            }
            catch (IllegalFlightParameter ex)
            {
                return StatusCode(400, $"{{ error: \"{ex.Message}\" }}"); // 400 + body = ex.Message
                //return BadRequest($"{{ error: {ex.Message} }}"); // 400 + body = ex.Message
            }
            //return StatusCode(200, result);
            if (result == null)
            {
                return StatusCode(204, "{ }");
            }
            return Ok(result);
        }

        [HttpPost("purchaseticket/{flight_id}")]
        public void PurchaseTickets(int flight_id) // TOKEN
        {
            // 1. AuthenticateAndGetTokenAndGetFacade()
            // 2. var result = customerFacade.GetAllTickets(token);
            // 3. return resut;
            AuthenticateAndGetTokenAndGetFacade(out LoginToken<Customer> token_customer, out CustomerFacade facade);
            facade.PurchaseTicket(flight_id);
        }


        [HttpGet("getallflights")]
        public string GetAllFlights() // TOKEN
        {
            // 1. AuthenticateAndGetTokenAndGetFacade()
            // 2. var result = customerFacade.GetAllFlights(token?);
            // 3. return resut;
            return "value";
        }

    }
}
