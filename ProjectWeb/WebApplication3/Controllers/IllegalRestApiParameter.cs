﻿using System;
using System.Runtime.Serialization;

namespace WebApplication3.Controllers
{
    [Serializable]
    internal class IllegalRestApiParameter : Exception
    {
        public IllegalRestApiParameter()
        {
        }

        public IllegalRestApiParameter(string message) : base(message)
        {
        }

        public IllegalRestApiParameter(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected IllegalRestApiParameter(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}