﻿using System;
using System.Runtime.Serialization;

namespace projpart1
{
    [Serializable]
    public class IllegalFlightParameter : Exception
    {
        public IllegalFlightParameter()
        {
        }

        public IllegalFlightParameter(string message) : base(message)
        {
        }

        public IllegalFlightParameter(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected IllegalFlightParameter(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}